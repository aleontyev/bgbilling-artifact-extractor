package com.tfpbx.bgbilling.exctrator;

import com.tfpbx.bgbilling.exctrator.dto.Information;
import com.tfpbx.bgbilling.exctrator.dto.ModuleInfo;
import org.apache.maven.cli.MavenCli;

import java.io.File;
import java.io.FileOutputStream;
import java.text.MessageFormat;

public class ArchivaDeployer {

    private static final String SCRIPT_FILENAME = "deployer.bat";
    private static final String DEPLOY_CMD_PATTERN = "deploy:deploy-file"
            + " -Dfile={0}"
            + " -DrepositoryId={1}"
            + " -Durl={2}"
            + " -DartifactId={3}"
            + " -DgroupId={4}"
            + " -Dversion={5}";

    public ArchivaDeployer() {

    }

    public void deployArtifact(ModuleInfo moduleInfo, String version) throws Exception {
        Information information = moduleInfo.getModule().getInformation();
        deployArtifact(moduleInfo.getFile().getAbsolutePath(), information.getName(), "ru.bitel.bgbilling." + information.getType() + "s",
                version);
    }

    public void deployArtifact(String filename, String artifactId, String groupId, String version) throws Exception {
        System.out.println(System.setProperty("maven.multiModuleProjectDirectory", "./"));
        MavenCli cli = new MavenCli();
        String shellCommand = MessageFormat.format(DEPLOY_CMD_PATTERN, filename, "bgbilling-community-deploy",
                "http://91.204.234.250/repository/bgbilling", artifactId, groupId, version);
        System.out.println("shellCommand = " + shellCommand);
        cli.doMain(new String[]{"-version"}, ".", null, null);
        cli.doMain(new String[]{shellCommand}, ".", null, null);
    }

    public void writeCommandToFile(String command) throws Exception {
        File script = new File(SCRIPT_FILENAME);
        FileOutputStream fos = new FileOutputStream(script, true);
        fos.write((command + "\r\n").getBytes());
        fos.close();
    }

    public String getCommand(ModuleInfo moduleInfo, String version) throws Exception {
        Information information = moduleInfo.getModule().getInformation();
        String shellCommand = MessageFormat.format(DEPLOY_CMD_PATTERN, moduleInfo.getFile().getAbsolutePath(), "bgbilling-community-deploy",
                "http://91.204.234.250/repository/bgbilling", formatName(information.getName()), "ru.bitel.bgbillng." + information.getType() + "s", version);
        return "call mvn " + shellCommand;
    }

    private String formatName(String name) {
        String[] split = name.split("\\.");
        if(split.length < 2) {
            return name;
        }
        return split[split.length-1];
    }
}

