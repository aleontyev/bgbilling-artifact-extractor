package com.tfpbx.bgbilling.exctrator;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FtpDownloader {
    private String server;
    private FTPClient ftpClient = new FTPClient();

    public FtpDownloader(String server) throws IOException {
        this.server = server;
        ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
        ftpClient.connect(this.server, 21);
        ftpClient.login("anonymous", "");
    }

    public List<String> listDir(String dir) {
        try {
            return Arrays.stream(ftpClient.listDirectories(dir))
                    .map(FTPFile::getName)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return Collections.emptyList();
    }

    public List<String> listFilesInDir(String dir) {
        try {
            return Arrays.stream(ftpClient.listFiles(dir))
                    .map(FTPFile::getName)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return Collections.emptyList();
    }

    public String saveFile(String remotePath, String localPath, String filename) {
        File targetFolder = new File(localPath);
        if(!targetFolder.exists()) {
            targetFolder.mkdirs();
        }

        String filepath = localPath + "/" + filename;
        try (OutputStream outputStream = new FileOutputStream(new File(filepath))) {
            ftpClient.retrieveFile(remotePath + "/" + filename, outputStream);
            outputStream.close();
            return filepath;
        } catch (Exception e) {
            System.out.println("saveFile failed: " + e);
            e.printStackTrace(System.out);
        }
        return null;
    }
}
