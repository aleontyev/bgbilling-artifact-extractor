package com.tfpbx.bgbilling.exctrator;

import com.tfpbx.bgbilling.exctrator.dto.ModuleInfo;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Predicate;

public class ZipFinder {
    private static final String PATH_TO_SAVE = "target/jars/";
    private static final String TMP_PATH = "target/jars/extracted/";
    private static final String FTP_SERVER = "bgbilling.ru";
    private static final String REMOTE_ROOT = "/pub/bgbilling/";
    private static final String[] VERSIONS = {"6.0", "6.1", "6.2", "7.0", "7.1", "7.2"};
//    private static final String[] VERSIONS = {"7.0"};


    public static void main(String[] args) throws Exception {
        final FtpDownloader ftpDownloader = new FtpDownloader(FTP_SERVER);
        final ArchivaDeployer archivaDeployer = new ArchivaDeployer();

        final Predicate<String> fileFilter = filename ->
                filename.endsWith(".zip") &&
                        !filename.substring(0, 1).equals(filename.substring(0, 1).toUpperCase());

        Arrays.stream(VERSIONS).forEach(
                version -> ftpDownloader.listFilesInDir(REMOTE_ROOT + version).stream()
                        .filter(fileFilter)
                        .map(filename -> ftpDownloader.saveFile(REMOTE_ROOT + version, PATH_TO_SAVE + version, filename))
                        .map(ZipExtractor::new)
                        .map(zipExtractor -> {
                            try {
                                return zipExtractor.processZipArchive(TMP_PATH + version + "/");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return new ModuleInfo(null, null);
                        })
                        .forEach(moduleInfo -> {
                            try {
                                archivaDeployer.writeCommandToFile(archivaDeployer.getCommand(moduleInfo, version));
                            } catch (Exception e) {
                                System.out.println("failed to upload " + moduleInfo);
                                e.printStackTrace();
                            }
                        })
        );

    }

}
