package com.tfpbx.bgbilling.exctrator;

import com.tfpbx.bgbilling.exctrator.dto.Information;
import com.tfpbx.bgbilling.exctrator.dto.Module;
import com.tfpbx.bgbilling.exctrator.dto.ModuleInfo;
import com.tfpbx.bgbilling.exctrator.dto.TranslateableParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipExtractor {
    private static final Logger logger = LoggerFactory.getLogger(ZipExtractor.class);

    private String zipFilePath;


    public ZipExtractor(String zipFilePath) {
        this.zipFilePath = zipFilePath;
    }

    public ModuleInfo processZipArchive(String path) throws IOException, URISyntaxException {
        logger.debug("path: {}", path);
        Path targetDirPath = Paths.get(path, Paths.get(zipFilePath).getFileName().toString().replace(".zip", "" ));
        Files.createDirectories(targetDirPath);
        final ModuleInfo moduleInfo = new ModuleInfo();

        try(FileSystem zipFs = createZipFileSystem(zipFilePath, false)) {
            Path pathInZip = zipFs.getPath("/");
            logger.debug("pathInZip: {}", pathInZip);
            Files.walkFileTree(pathInZip, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
                    if(!filePath.getFileName().endsWith("module.xml") &&
                            !filePath.getFileName().endsWith("module.properties") &&
                            !filePath.getFileName().endsWith("server.zip")) {
                        return FileVisitResult.CONTINUE;
                    }
                    // Make sure that we conserve the hierachy of files and folders inside the zip
                    Path targetPath = Paths.get(targetDirPath.toString(), filePath.toString());
                    // And extract the file
                    Files.copy(filePath, targetPath, StandardCopyOption.REPLACE_EXISTING);

                    if(filePath.getFileName().endsWith("module.xml")) {
                        try {
                            File moduleXmlFile = new File(targetPath.toUri());
                            Module module = JAXB.unmarshal(moduleXmlFile, Module.class);
                            logger.debug("module from xml: {}", module);
                            moduleInfo.setModule(module);
                            boolean delete = (new File(targetPath.toUri().toString())).delete();
                        } catch (Exception e) {
                            logger.error("error while read module.xml: {}", e.getMessage() , e);
                        }

                    }

                    if(filePath.getFileName().endsWith("module.properties")) {
                        try {
                            InputStream propertiesStream = new FileInputStream(new File(targetPath.toUri()));
                            Properties moduleProperties = new Properties();
                            moduleProperties.load(propertiesStream);
                            Module module = getModuleInfo(moduleProperties);
                            logger.debug("module from properties: {}", module);
                            moduleInfo.setModule(module);
                            boolean delete = (new File(targetPath.toUri().toString())).delete();
                        } catch (Exception e) {
                            logger.error("error while read module.xml: {}", e.getMessage() , e);
                        }

                    }

                    if(filePath.getFileName().endsWith("server.zip")) {
                        try(FileSystem serverZipFs = createZipFileSystem(targetPath.toString(), false)) {
                            Path pathInZip = serverZipFs.getPath("/");
                            Files.walkFileTree(pathInZip, new SimpleFileVisitor<Path>() {
                                @Override
                                public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
                                    if(!filePath.getFileName().toString().endsWith("jar")) {
                                        return FileVisitResult.CONTINUE;
                                    }
                                    // Make sure that we conserve the hierachy of files and folders inside the zip
                                    Path jarTargetPath = Paths.get(targetDirPath.toString(), filePath.toString());
                                    // And extract the file
                                    Files.copy(filePath, jarTargetPath, StandardCopyOption.REPLACE_EXISTING);

                                    if(filePath.getFileName().toString().endsWith("jar")) {
                                        moduleInfo.setFile(new File(jarTargetPath.toUri()));
                                        logger.debug("got file: {}", filePath);
                                        return FileVisitResult.TERMINATE;
                                    }
                                    return FileVisitResult.CONTINUE;
                                }
                            });
                        } catch (Exception e) {
                            logger.error("error while read server.zip: {}", e.getMessage() , e);

                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (Exception e) {
            logger.error("error while create feilesysten: {}", e.getMessage(), e);
        }

        logger.debug("moduleInfo: {}", moduleInfo );
        return moduleInfo;
    }

    private Module getModuleInfo(Properties moduleProperties) {
        Module module = new Module();
        Information information = new Information();
        information.setVersion(moduleProperties.getProperty("module.version"));
        information.setName(moduleProperties.getProperty("name"));
        information.setType(moduleProperties.getProperty("type"));
        List<TranslateableParam> title = Collections.singletonList(new TranslateableParam(moduleProperties.getProperty("title")));
        information.setTitle(title);
        module.setInformation(information);
        return module;
    }

    private static FileSystem createZipFileSystem(String zipFilename, boolean create) throws IOException {
        final Path path = Paths.get(zipFilename);
        final URI uri = URI.create("jar:file:" + path.toUri().getPath());
        logger.debug("URI: {}", uri);
        final Map<String, String> env = new HashMap<>();
        if (create) {
            env.put("create", "true");
        }
        return FileSystems.newFileSystem(uri, env);
    }

}
