package com.tfpbx.bgbilling.exctrator.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ZipDescriptor {
    @XmlElement(name = "module")
    private Module module;

}
