package com.tfpbx.bgbilling.exctrator.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Call implements Serializable {
    @XmlAttribute(name = "class")
    private String clazz;
    @XmlAttribute(name = "param")
    private String param;

    public Call() {
    }

    public Call(String clazz, String param) {
        this.clazz = clazz;
        this.param = param;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Call{");
        sb.append("clazz='").append(clazz).append('\'');
        sb.append(", param='").append(param).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
