package com.tfpbx.bgbilling.exctrator.dto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TranslateableParam implements Serializable {
    @XmlAttribute(name = "lang")
    private String lang = "en";

    @XmlValue
    private String value;

    public TranslateableParam() {
    }

    public TranslateableParam(String lang) {
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TranslateableParam{");
        sb.append("lang='").append(lang).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
