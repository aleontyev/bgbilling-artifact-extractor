package com.tfpbx.bgbilling.exctrator.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Module implements Serializable {
    private Information information;
    @XmlElementWrapper(name = "resources")
    private List<Call> call;

    public Module() {
    }

    public Information getInformation() {
        return information;
    }

    public void setInformation(Information information) {
        this.information = information;
    }

    public List<Call> getCalls() {
        return call;
    }

    public void setCalls(List<Call> call) {
        this.call = call;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Module{");
        sb.append("information=").append(information);
        sb.append(", call=").append(call);
        sb.append('}');
        return sb.toString();
    }
}
