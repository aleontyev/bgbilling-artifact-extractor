package com.tfpbx.bgbilling.exctrator.dto;

import java.io.File;

public class ModuleInfo {
    private Module module;
    private File file;

    public ModuleInfo() {
    }

    public ModuleInfo(Module module, File file) {
        this.module = module;
        this.file = file;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ModuleInfo{");
        sb.append("module=").append(module);
        sb.append(", file=").append(file);
        sb.append('}');
        return sb.toString();
    }
}
