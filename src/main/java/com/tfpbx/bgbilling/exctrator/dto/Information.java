package com.tfpbx.bgbilling.exctrator.dto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Information implements Serializable {
    @XmlAttribute
    private String type;
    @XmlAttribute
    private String version;
    @XmlAttribute
    private String name;
    @XmlAttribute(name = "pack_server")
    private String packServer;
    @XmlAttribute(name = "pack_client")
    private String packClient;
    @XmlElement
    private List<TranslateableParam> vendor;
    @XmlElement
    private List<TranslateableParam> title;
    @XmlElement
    private List<TranslateableParam> description;

    public Information() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackServer() {
        return packServer;
    }

    public void setPackServer(String packServer) {
        this.packServer = packServer;
    }

    public String getPackClient() {
        return packClient;
    }

    public void setPackClient(String packClient) {
        this.packClient = packClient;
    }

    public List<TranslateableParam> getVendor() {
        return vendor;
    }

    public void setVendor(List<TranslateableParam> vendor) {
        this.vendor = vendor;
    }

    public List<TranslateableParam> getTitle() {
        return title;
    }

    public void setTitle(List<TranslateableParam> title) {
        this.title = title;
    }

    public List<TranslateableParam> getDescription() {
        return description;
    }

    public void setDescription(List<TranslateableParam> description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Information{");
        sb.append("type='").append(type).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", packServer='").append(packServer).append('\'');
        sb.append(", packClient='").append(packClient).append('\'');
        sb.append(", vendor=").append(vendor);
        sb.append(", title=").append(title);
        sb.append(", description=").append(description);
        sb.append('}');
        return sb.toString();
    }
}
