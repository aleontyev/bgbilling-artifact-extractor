package com.tfpbx.bgbilling.exctrator.dto;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.junit.Test;

import javax.xml.bind.JAXB;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ModuleTest {

    @Test
    public void generateTestModuleXml() throws Exception {
        Module module =  new Module();
        Information information = new Information();
        information.setType("type");
        information.setVersion("version");
        information.setName("rscm");
        List<Call> callList = new ArrayList<>();
        callList.add(new Call("clazz1", "param1"));
        callList.add(new Call("clazz2", "param2"));
        module.setCalls(callList);
        TranslateableParam en = new TranslateableParam("en");
        en.setValue("en = value");
        information.setVendor(Collections.singletonList(en));
        module.setInformation(information);
        JAXB.marshal(module,System.out);

    }

}